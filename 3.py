import platform

def strong():
    count = int(input ("Please enter how many number must be  "))
    gamenumber = list (range(1, count+1))
    print ("So. The following numbers take part in the game ", gamenumber, " Let's play. Imagine some nuber from the list and I'll try to guess")
    while True:
        guessnumber = (int(gamenumber[(len(gamenumber)//2)]))
        print ("Is Your imagined number  ",guessnumber, " ? Please enter your answer YES/MORE/LESS")
        answer = input ()
        if answer == "more":
            if len(gamenumber)//2 != 0:
                gamenumber = gamenumber[len(gamenumber)//2:]  
                print ("Ok. Let's play further. Now in the game take part next numbers", gamenumber)
            else:
                print ("Funny trick. Your number was ", gamenumber)
                break
        elif answer == "less":
            if len(gamenumber)//2 != 0:
                gamenumber = gamenumber[:len(gamenumber)//2] 
                print ("Ok. Let's play further. Now in the game take part next numbers", gamenumber) 
            else:
                print ("Funny trick. Your number was ", gamenumber)
                break
        elif answer == "yes":
            print ("Hey! I'm smart :)")
            break
        else:   
            input ("Your choise isn't right. Please enter again  ")


def clasic():
    enterednubmer = (input ("Please enter numbers from 0 to 9 without any separator, letters or any specifical symbols:  "))
    while len(enterednubmer) == 0:
        enterednubmer = (input ("You still have't entered anything. Try again :  "))
    gamenumber = [s for s in enterednubmer if s.isdigit()]
    print ("The numbers in the game ",gamenumber)
    print ("So, now imagine a number from the list, and I'll try to guess it")
    gamenumber = set(gamenumber)
    gamenumber = sorted(gamenumber)
    while True:
        guessnumber = (int(gamenumber[len(gamenumber)//2]))
        print ("Is Your imagined number  ", guessnumber, " ? Please enter your answer YES/MORE/LESS")
        answer = input ()
        if answer == "more":
            if len(gamenumber)//2 != 0:
                gamenumber = gamenumber[len(gamenumber)//2:]  
                print ("Ok. Let's play further. Now in the game take part next numbers", gamenumber)
            else:
                print ("Funny trick. Your number was ", gamenumber)
                break 
        elif answer == "less":
            if len(gamenumber)//2 != 0:
                gamenumber = gamenumber[:len(gamenumber)//2] 
                print ("Ok. Let's play further. Now in the game take part next numbers", gamenumber) 
            else:
                print ("Funny trick. Your number was ", gamenumber)
                break
        elif answer == "yes":
            print ("Hey! I'm smart :)")
            break
        else:   
            input ("Your choise isn't right. Please enter again\t")

def thegame():
    username = input ("Set your name: ")
    print ("Hello " + username + "\nMy name is Python " + platform.python_version() + "\nDo you want to play the game with me?")
    answer = (input ("Please choose the level of the game. \nEnter \'1\' for \'Clasical\' mode and \'2\' for \'Strong\'. Or press any key for exit  "))
    if answer == "1":
        print ("Your choise is \'Clasic\', so let's start")
        clasic()
    elif answer == "2":
        print ("Your choise is \'Strong\', so let's start")
        strong()
    else:
        print ("Well, sorry. See you later")

if __name__ == "__main__":
    thegame()
else:
    print ("Please return to your real environment")