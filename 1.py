import platform
import sys
if hasattr(sys, 'real_prefix'):
    env = "Virtual Environment"
else:
    env = "Real Environment"
print ("Hello dear user! \nYou are using: \nOS: %s " % platform.system() + "\nType: %s \nVersion: %s \nCode Name: %s" % platform.dist() + 
"\nPython Version: %s" % sys.version[:6] + "\nEnvironment Type: " + env)